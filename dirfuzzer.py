import re
import sys
import requests
from concurrent.futures import ThreadPoolExecutor, thread

def make_wordlist(wordlist):
    with open(wordlist) as wd:
        return [word.strip() for word in wd if not word.startswith('#') and len(word.split())>0]

def make_url(url, wordlist):
    # i don't like pool
    try:
        with ThreadPoolExecutor(max_workers=3) as LAKE:
            for word in make_wordlist(wordlist):
                if url[-1] != '/':
                    url += '/'
                # create new url+word for the request
                requrl = url + word
                future = LAKE.submit(check_url, requrl)
    except KeyboardInterrupt:
        LAKE._threads.clear()
        thread._threads_queues.clear()
        sys.exit('\n******* brutal exit *******')

def check_url(url):
    check_page = requests.get(url)
    # easy regex for directory listing detection
    dirlisting = re.findall(r'<head>\s+<title>Index of',
                            check_page.content.decode('utf-8'))
    if len(dirlisting) > 0:
        # if dirlisting was found print link and go on ---
        print('[+][+][+] found directory listing on:', url)
    if check_page.status_code == 200:
        # dir found, need depth -------------------------------------------------
        print('[+] found something in:', url)
        # very dumb
        if url[-1] != '/':
            make_url(url+'/', wordlist)


def check_start(site, wordlist):    
    
    try:
        # if there's an error maybe the link is wrong/incomplete
        code_request = requests.get(site).status_code
    except:
        # try to solve it with very dumb method
        site = 'http://' + site
        code_request = requests.get(site).status_code

    if code_request == 200:
        make_url(site, wordlist)
    elif code_request == 400:
        print("it's seems to be offline")
    else:
        print('error, response code:', code_request)


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print('Usage: python3 dirfuzzer.py http://10.10.10.10 wordlist')
        exit()
    
    target = sys.argv[1]
    try:
        wordlist = sys.argv[2]
    except IndexError:
        wordlist = 'wordlist.txt'

    check_start(target, wordlist)
